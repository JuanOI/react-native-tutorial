import { Image, Text, View, StyleSheet } from "react-native";
import PrimaryButton from "../components/ui/PrimaryButton";
import Title from "../components/ui/Title";
import Colors from "../constants/Colors";

function GameOverScreen({ roundsNumber, userNumber, onStartNewGame }) {
  return (
    <View style={styles.rootContainer}>
      <Title> Game Over! </Title>
      <View style={styles.imageContainer}>
        <Image
          source={require("../assets/images/success.png")}
          style={styles.image}
        />
      </View>
      <Text style={styles.summaryText}>
        {" "}
        <Text style={styles.highlight}>{roundsNumber}</Text> rounds were needed
        to guess the number <Text style={styles.highlight}>{userNumber}</Text>
      </Text>
      <PrimaryButton onPress={onStartNewGame}>Start New Game</PrimaryButton>
    </View>
  );
}

export default GameOverScreen;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    padding: 24,
    justifyContent: "center",
    alignItems: "center",
  },
  imageContainer: {
    width: 300,
    height: 300,
    borderRadius: 150,
    borderWidth: 3,
    margin: 36,
    borderColor: Colors.primary800,
    overflow: "hidden",
  },
  image: {
    width: "95%",
    height: "95%",
  },
  summaryText: {
    fontFamily: "Inter-SemiBold",
    fontSize: 22,
    textAlign: "center",
    marginVertical: 24,
  },
  highlight: {
    fontFamily: "Inter-SemiBold",
    color: Colors.primary500,
  },
});
