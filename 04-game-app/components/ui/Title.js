import { StyleSheet, Text } from "react-native";
import Colors from "../../constants/Colors";

function Title({children}) {
  return (
    <Text style={styles.title}>
      {children}
    </Text>
  );
}

export default Title;

const styles = StyleSheet.create({
  title: {
    fontFamily: "Inter-SemiBold",
    fontSize: 24,
    fontWeight: "bold",
    color: "white",
    textAlign: "center",
    borderWidth: 2,
    borderColor: "white",
    padding: 12
  }
});