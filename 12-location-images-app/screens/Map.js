import { useCallback, useLayoutEffect, useState } from "react";
import { Alert, StyleSheet } from "react-native";
import MapView, { Marker } from "react-native-maps";
import IconButton from "../components/UI/IconButton";

function Map({ navigation, route }) {
  const providedLocation = route.params && {
    latitude: route.params.lat,
    longitude: route.params.lon,
  }; 

  const [selectedLocation, setSelectedLocation] = useState(providedLocation);

  const region = {
    latitude: providedLocation ? providedLocation.latitude : 37.78,
    longitude: providedLocation ? providedLocation.longitude : -122.43,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  };

  function selectedLocationHandler(event) {
    if(providedLocation){
      return;
    }
    const coordinate = { ...event.nativeEvent.coordinate };
    setSelectedLocation(coordinate);
  }

  const savePickedLocationHandler = useCallback(() => {
    if (!selectedLocation) {
      Alert.alert("No picked location", "You need to pick a location!");
      return;
    }

    navigation.navigate("AddPlace", {
      pickedLat: selectedLocation.latitude,
      pickedLon: selectedLocation.longitude,
    });
    
  });

  useLayoutEffect(() => {
    if(!providedLocation){
      navigation.setOptions({
        headerRight: ({ tintColor }) => (
          <IconButton
            icon="checkmark"
            size={32}
            color={tintColor}
            onPress={savePickedLocationHandler}
          />
        ),
      });
    }
  }, [providedLocation, navigation, savePickedLocationHandler]);

  return (
    <MapView
      style={styles.map}
      initialRegion={region}
      onPress={selectedLocationHandler}
    >
      {selectedLocation && (
        <Marker title="Selected Location" coordinate={selectedLocation} />
      )}
    </MapView>
  );
}

export default Map;

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
});
