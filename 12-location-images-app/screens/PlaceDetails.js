import { NavigationHelpersContext } from "@react-navigation/native";
import { useEffect, useState } from "react";
import { Image, ScrollView, StyleSheet, Text, View } from "react-native";
import OutlinedButton from "../components/UI/OutlinedButton";
import { Colors } from "../constants/colors";
import { fetchPlaceDetails } from "../util/db";

function PlaceDetails({ route, navigation }) {
  const [placeInfo, setPlaceInfo] = useState(null);

  function showOnMapHandler() {
    navigation.navigate("Map", {
      lat: placeInfo.location.lat,
      lon: placeInfo.location.lon,
    });
  }

  const placeId = route.params.placeId;

  useEffect(() => {
    async function syncFetchPlaceDetails() {
      const placeDetails = await fetchPlaceDetails(placeId);
      setPlaceInfo(placeDetails);
      navigation.setOptions({
        title: placeDetails.title,
      });
    }

    syncFetchPlaceDetails();
  }, [placeId, setPlaceInfo]);

  if (!placeInfo) {
    return (
      <View style={styles.fallback}>
        <Text>Loading data...</Text>
      </View>
    );
  }

  return (
    <ScrollView>
      <Image style={styles.image} source={{ uri: placeInfo.imageUri }} />
      <View style={styles.locationContainer}>
        <View style={styles.addressContainer}>
          <Text style={styles.address}>{placeInfo.address}</Text>
        </View>
        <OutlinedButton icon="map" onPress={showOnMapHandler}>
          View on Map
        </OutlinedButton>
      </View>
    </ScrollView>
  );
}

export default PlaceDetails;

const styles = StyleSheet.create({
  fallback: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    height: "35%",
    minHeight: 300,
    width: "100%",
  },
  locationContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  address: {
    color: Colors.primary500,
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 16,
  },
  addressContainer: {
    padding: 20,
  },
});
