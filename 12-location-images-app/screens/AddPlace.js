import PlaceForm from "../components/Places/PlaceForm";
import { insertPlace } from "../util/db";

function AddPlace({ navigation }) {
  async function addPlaceHandler(place) {
    await insertPlace(place);
    navigation.navigate("AllPlaces");
  }

  return <PlaceForm onAddPlace={addPlaceHandler} />;
}

export default AddPlace;
