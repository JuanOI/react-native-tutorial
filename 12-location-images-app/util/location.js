import { GOOGLE_API_KEY } from "@env";

// https://developers.google.com/maps/documentation/maps-static/overview
export function getMapPreview(lat, lon) {
  const mapUrl = `https://maps.googleapis.com/maps/api/staticmap?key=${GOOGLE_API_KEY}&center=${lat},${lon}&zoom=14&size=400x200&maptype=roadmap&markers=color:red%7Clabel:C%7C${lat},${lon}`;
  return mapUrl;
}

// https://developers.google.com/maps/documentation/geocoding/requests-geocoding
export async function getAddress(lat, lon) {
  const url = `https://maps.googleapis.com/maps/api/geocode/json?key=${GOOGLE_API_KEY}&latlng=${lat},${lon}`;
  
  const address = fetch(url)
    .then((response) => response.json())
    .then((data) => data.results[0].formatted_address)
    .catch((error) => {
      console.log(error);
      return "Unknown";
    });

  return address;
}
