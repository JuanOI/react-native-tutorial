import {
  launchCameraAsync,
  useCameraPermissions,
  PermissionStatus,
} from "expo-image-picker";
import { useState } from "react";
import { Alert, Image, StyleSheet, Text, View } from "react-native";
import { Colors } from "../../constants/colors";
import OutlinedButton from "../UI/OutlinedButton";

function ImagePicker({onTakeImage}) {
  const [pickedImageUri, setPickedImageUri] = useState();

  const [cameraPermissionsInfo, requestPermissions] = useCameraPermissions();

  async function verifyPermissions() {
    let permissionsGranted;
    switch (cameraPermissionsInfo.status) {
      case PermissionStatus.UNDETERMINED:
        const requestResponse = await requestPermissions();
        permissionsGranted = requestResponse.granted;
        break;
      case PermissionStatus.DENIED:
        Alert.alert(
          "Insufficent Permissions",
          "You need to grant camera access to use this app!"
        );
        permissionsGranted = false;
      default:
        permissionsGranted = true;
        break;
    }

    return permissionsGranted;
  }

  async function takeImageHandler() {
    const hasPermissions = await verifyPermissions();

    if (!hasPermissions) {
      return;
    }

    const image = await launchCameraAsync({
      allowsEditing: true,
      aspect: [16, 9],
      quality: 0.25,
    });
    console.log(image);
    setPickedImageUri(image.uri);
    onTakeImage(image.uri)
  }

  let imagePreview = <Text>No image available</Text>;
  if (pickedImageUri) {
    imagePreview = (
      <Image style={styles.image} source={{ uri: pickedImageUri }} />
    );
  }

  return (
    <View>
      <View style={styles.imageContainer}>{imagePreview}</View>
      <OutlinedButton icon="camera" onPress={takeImageHandler}>
        Take Image
      </OutlinedButton>
    </View>
  );
}

export default ImagePicker;

const styles = StyleSheet.create({
  imageContainer: {
    width: "100%",
    height: 200,
    marginVertical: 8,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Colors.primary100,
    borderRadius: 2,
  },
  image: {
    width: "100%",
    height: "100%",
  },
});
