import { Alert, Image, StyleSheet, View, Text } from "react-native";
import {
  getCurrentPositionAsync,
  useForegroundPermissions,
  PermissionStatus,
} from "expo-location";
import {
  useIsFocused,
  useNavigation,
  useRoute,
} from "@react-navigation/native";

import { Colors } from "../../constants/colors";
import { getAddress, getMapPreview } from "../../util/location";
import OutlinedButton from "../UI/OutlinedButton";
import { useEffect, useState } from "react";

function LocationPicker({ onPickLocation }) {
  const [pickedLocation, setPickedLocation] = useState();
  const isFocused = useIsFocused();

  const navigation = useNavigation();
  const route = useRoute();

  const [locationPermissionsInfo, requestPermissions] =
    useForegroundPermissions();

  useEffect(() => {
    if (isFocused && route.params) {
      const mapPickedLocation = route.params && {
        lat: route.params.pickedLat,
        lon: route.params.pickedLon,
      };

      setPickedLocation(mapPickedLocation);
    }
  }, [route, isFocused, setPickedLocation]);

  useEffect(() => {
    async function handleLocation() {
      if (pickedLocation) {
        const address = await getAddress(
          pickedLocation.lat,
          pickedLocation.lon
        );
        onPickLocation({ ...pickedLocation, address: address });
      }
    }
    handleLocation();
  }, [pickedLocation, onPickLocation]);

  async function verifyPermissions() {
    let permissionsGranted;
    switch (locationPermissionsInfo.status) {
      case PermissionStatus.UNDETERMINED:
        const requestResponse = await requestPermissions();
        permissionsGranted = requestResponse.granted;
        break;
      case PermissionStatus.DENIED:
        Alert.alert(
          "Insufficent Permissions",
          "You need to grant location access to use this app!"
        );
        permissionsGranted = false;
      default:
        permissionsGranted = true;
        break;
    }

    return permissionsGranted;
  }

  async function getLocationHandler() {
    const hasPermissions = await verifyPermissions();
    if (!hasPermissions) {
      return;
    }

    const location = await getCurrentPositionAsync();
    console.log(location);
    setPickedLocation({
      lat: location.coords.latitude,
      lon: location.coords.longitude,
    });
  }
  function pickLocationHandler() {
    navigation.navigate("Map");
  }

  let locationPreview = <Text>No location available</Text>;
  if (pickedLocation) {
    locationPreview = (
      <Image
        style={styles.image}
        source={{ uri: getMapPreview(pickedLocation.lat, pickedLocation.lon) }}
      />
    );
  }

  return (
    <View>
      <View style={styles.mapPreview}>{locationPreview}</View>
      <View style={styles.actions}>
        <OutlinedButton icon="location" onPress={getLocationHandler}>
          Use location
        </OutlinedButton>
        <OutlinedButton icon="map" onPress={pickLocationHandler}>
          Select location
        </OutlinedButton>
      </View>
    </View>
  );
}

export default LocationPicker;

const styles = StyleSheet.create({
  mapPreview: {
    width: "100%",
    height: 200,
    marginVertical: 8,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Colors.primary100,
    borderRadius: 2,
    overflow: "hidden",
  },
  actions: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignContent: "center",
  },
  image: {
    width: "100%",
    height: "100%",
  },
});
