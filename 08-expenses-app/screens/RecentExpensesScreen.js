import { useContext, useEffect, useState } from "react";
import ExpensesOutput from "../components/ExpensesOutput/ExpensesOutput";
import ErrorOverlay from "../components/UI/ErrorOverlay";
import LoadingOverlay from "../components/UI/LoadingOverlay";
import { ExpensesContext } from "../store/expenses-context";
import { getDateMinusDelta } from "../util/date";
import { fetchExpenses } from "../util/http";

function RecentExpensesScreen() {
  const [error, setError] = useState(null);
  const [isFetching, setIsFetching] = useState(true);

  const expensesContext = useContext(ExpensesContext);

  useEffect(() => {
    async function getExpenses() {
      setIsFetching(true);
      try {
        const expenses = await fetchExpenses();
        expensesContext.setExpenses(expenses);
      } catch (error) {
        setError("Could not fetch expenses.");
      }
      setIsFetching(false);
    }

    getExpenses();
  }, []);

  function errorHandler(){
    setError(null);
  }

  if (error && !isFetching) {
    return <ErrorOverlay message={error} onConfirm={errorHandler}/>;
  }

  if (isFetching) {
    return <LoadingOverlay />;
  }

  const recentExpenses = expensesContext.expenses.filter((expense) => {
    const dateToday = new Date();
    const dateSevenDaysAgo = getDateMinusDelta(dateToday, 7);
    return expense.date > dateSevenDaysAgo;
  });
  return (
    <ExpensesOutput
      expenses={recentExpenses}
      period="Last 7 days"
      fallbackText="No expenses registered recently!"
    />
  );
}

export default RecentExpensesScreen;
