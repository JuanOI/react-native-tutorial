export function getFormattedDate(date) {
  return date.toISOString().split("T")[0];
}

export function getDateMinusDelta(date, delta) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate() - delta);
}
